package com.ing.spendanalyzer.spendanalyzer.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.ing.spendanalyzer.spendanalyzer.controller.impl.LoginControllerImpl;
import com.ing.spendanalyzer.spendanalyzer.dto.CustomerDto;
import com.ing.spendanalyzer.spendanalyzer.dto.ResponseDto;
import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;
import com.ing.spendanalyzer.spendanalyzer.model.Customer;
import com.ing.spendanalyzer.spendanalyzer.service.LoginService;

@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {
	
	@InjectMocks
	private LoginControllerImpl loginControllerImpl;

	@Mock
	private LoginService loginService;
	Customer customer;
	Customer customerDB;
	CustomerDto custDto;
	@SuppressWarnings("rawtypes")
	ResponseDto response;
	@Mock
	BindingResult bindingResult;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Before
	public void setUp() {
		customer = new Customer();
		customerDB = new Customer();
		custDto = new CustomerDto();
		response = new ResponseDto();
		
		customer.setCustomerId(100);
		customer.setLoginId("hello1");
		customer.setName("Hello");
		customer.setPassword("Hello123");
		
		customerDB.setCustomerId(100);
		customerDB.setLoginId("hello1");
		customerDB.setName("Hello");
		customerDB.setPassword("Hello456");
		
		
		custDto.setCustomerId(customer.getCustomerId());
		custDto.setLoginId(customer.getLoginId());
		custDto.setName(customer.getName());
		
		response.setData(custDto);
		response.setStatus(1);
	}
	
	@After
	public void tearDown() {
		
	}
	
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testCorrectLoginUserSuccess() throws Exception {

		Mockito.when(loginService.findByLoginId(customer.getLoginId())).thenReturn(customer);
		Assert.assertEquals(new ResponseEntity<ResponseDto>(response, HttpStatus.OK),loginControllerImpl.loginUser(customer,bindingResult));
	}
	
	@Test(expected=SpendAnalyzerException.class)
	public void testLoginUserIncorrectPwdFailure() throws Exception {
		Mockito.when(loginService.findByLoginId(customer.getLoginId())).thenReturn(customerDB);
		loginControllerImpl.loginUser(customer,bindingResult);
	}

}
