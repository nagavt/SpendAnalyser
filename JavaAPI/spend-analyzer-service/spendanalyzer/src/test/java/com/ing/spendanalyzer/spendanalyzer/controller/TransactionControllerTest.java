package com.ing.spendanalyzer.spendanalyzer.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.ing.spendanalyzer.spendanalyzer.controller.impl.TransactionControllerImpl;
import com.ing.spendanalyzer.spendanalyzer.dto.PaymentDto;
import com.ing.spendanalyzer.spendanalyzer.dto.ResponseDto;
import com.ing.spendanalyzer.spendanalyzer.dto.TransactionDto;
import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;
import com.ing.spendanalyzer.spendanalyzer.model.Transaction;
import com.ing.spendanalyzer.spendanalyzer.service.TransactionService;

@RunWith(MockitoJUnitRunner.class)
public class TransactionControllerTest {
	
	@InjectMocks
	private TransactionControllerImpl transactionControllerImpl;
	@Mock
	private TransactionService transService;
	List<Transaction> transactionList;
	ResponseDto<Object> response;
	Transaction transaction;
	TransactionDto transactionDto;
	PaymentDto paymentDto;
	@Mock
	BindingResult bindingResult;
	
	@Before
	public void setUp() {
		transactionList = new ArrayList<>();
		response = new ResponseDto<>();
		transaction = new Transaction();
		transactionDto = new TransactionDto();
		paymentDto = new PaymentDto();
		
		transaction.setAccountId(9001);
		transaction.setClosingBalance(10000.00);
		transaction.setPaymentType("DEBIT");
		transaction.setDescription("Loan EMI");
		transaction.setTransactionAmount(5000.00);
		transaction.setTransactionDate(new java.util.Date());
		transaction.setTransactionId(10001);
		
		paymentDto.setAmount(5000.00);
		paymentDto.setCustomerId(2334);
		paymentDto.setDescription("Loan EMI");
		paymentDto.setPaymentType("DEBIT");
		
		transactionDto.setAmount(5000.00);
		transactionDto.setClosingBalance(10000.00);
		transactionDto.setDescription("Loan EMI");
		transactionDto.setPaymentType("DEBIT");
		transactionDto.setTransactionDate(new java.util.Date());
		transactionDto.setTransactionId(10001);
		
		response.setStatus(1);
		response.setData(transactionList);
	}

	@Test
	public void testGetTransactions() throws Exception {
		Mockito.when(transService.getTransactionList(2334)).thenReturn(transactionList);
		Assert.assertEquals( new ResponseEntity<>(response, HttpStatus.OK), transactionControllerImpl.getTransactions(2334));
	}
	
	@Test
	public void testMakePayment() throws Exception {
		Mockito.when(transService.makePayment(paymentDto)).thenReturn(transactionDto);
		response.setData(transactionDto);
		Assert.assertEquals(new ResponseEntity<>(response, HttpStatus.OK),transactionControllerImpl.makePayment(paymentDto, bindingResult));
	}
}
