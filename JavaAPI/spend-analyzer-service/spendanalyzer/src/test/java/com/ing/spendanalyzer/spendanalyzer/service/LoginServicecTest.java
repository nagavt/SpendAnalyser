package com.ing.spendanalyzer.spendanalyzer.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.ing.spendanalyzer.spendanalyzer.model.Account;
import com.ing.spendanalyzer.spendanalyzer.model.Customer;
import com.ing.spendanalyzer.spendanalyzer.repository.CustomerRepository;
import com.ing.spendanalyzer.spendanalyzer.service.impl.LoginServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class LoginServicecTest {
	
	@InjectMocks
	LoginServiceImpl loginServiceImpl;
	@Mock
	CustomerRepository customerRepository;
	Customer customer;
	Account account;
	List<Account> accounts;
	
	@Before
	public void setUp(){
		customer = new Customer();
		account = new Account();
		accounts = new ArrayList<Account>();
		
		customer.setCustomerId(1001);
		customer.setLoginId("sahac");
		customer.setName("Chanchal Saha");
		customer.setPassword("chan123");
		customer.setAccounts(accounts);
		
		account.setAccountId(111220);
		account.setAccountNumber(999112);
		account.setCurrentBalance(50000.00);
		
	}
	
	@Test
	public void testFindByLoginIdSuccess() throws Exception{
		
		Mockito.when(customerRepository.findByLoginId(customer.getLoginId())).thenReturn(customer);
		
		Assert.assertEquals(customer,loginServiceImpl.findByLoginId(customer.getLoginId()));
		
		
	}
	
	@Test
	public void testFindByLoginIdFailure(){
		
		Mockito.when(customerRepository.findByLoginId(customer.getLoginId())).thenReturn(null);
		
		Assert.assertEquals(null, customerRepository.findByLoginId(customer.getLoginId()));
		
		
	}
}
