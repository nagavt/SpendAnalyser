package com.ing.spendanalyzer.spendanalyzer.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ing.spendanalyzer.spendanalyzer.constants.SpendAnalyzerConstants;
import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;
import com.ing.spendanalyzer.spendanalyzer.model.Customer;
import com.ing.spendanalyzer.spendanalyzer.repository.CustomerRepository;
import com.ing.spendanalyzer.spendanalyzer.service.LoginService;
import com.ing.spendanalyzer.spendanalyzer.util.LoggerUtil;
import com.ing.spendanalyzer.spendanalyzer.model.Error;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private CustomerRepository customerRepository;

	public Customer findByLoginId(String loginId) throws SpendAnalyzerException {
		LoggerUtil.info(SpendAnalyzerConstants.ENTRY + SpendAnalyzerConstants.EMPTY + LoginServiceImpl.class.getName()
				+ SpendAnalyzerConstants.EMPTY + Thread.currentThread().getStackTrace()[1].getMethodName());

		Error error = new Error();
		List<Error> errorList = new ArrayList<>();
		Customer customer = customerRepository.findByLoginId(loginId);

		if (customer == null) {
			error = new Error(HttpStatus.NOT_FOUND.toString(), "Please enter valid username and password");
			errorList.add(error);
			throw new SpendAnalyzerException(errorList, HttpStatus.OK.toString());

		}

		LoggerUtil.info(SpendAnalyzerConstants.EXIT + SpendAnalyzerConstants.EMPTY + LoginServiceImpl.class.getName()
				+ SpendAnalyzerConstants.EMPTY + Thread.currentThread().getStackTrace()[1].getMethodName());
		return customer;
	}

}
