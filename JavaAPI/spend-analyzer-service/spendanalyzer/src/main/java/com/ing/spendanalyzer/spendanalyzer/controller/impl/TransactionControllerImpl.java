package com.ing.spendanalyzer.spendanalyzer.controller.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ing.spendanalyzer.spendanalyzer.constants.SpendAnalyzerConstants;
import com.ing.spendanalyzer.spendanalyzer.controller.TransactionController;
import com.ing.spendanalyzer.spendanalyzer.dto.PaymentDto;
import com.ing.spendanalyzer.spendanalyzer.dto.ResponseDto;
import com.ing.spendanalyzer.spendanalyzer.dto.TransactionDto;
import com.ing.spendanalyzer.spendanalyzer.dto.TransactionSummaryDto;
import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;
import com.ing.spendanalyzer.spendanalyzer.model.Error;
import com.ing.spendanalyzer.spendanalyzer.model.Transaction;
import com.ing.spendanalyzer.spendanalyzer.service.TransactionService;
import com.ing.spendanalyzer.spendanalyzer.util.LoggerUtil;

/**
 * 
 * @author srikanth
 *
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class TransactionControllerImpl implements TransactionController {

	@Autowired
	private TransactionService transService;

	/**
	 * getTransactions --Get all the transactions for specific customer
	 * 
	 * @param customerId
	 * @return List of {@link ResponseDto} object
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/{customerId}/transactions")
	public ResponseEntity<ResponseDto> getTransactions(@PathVariable("customerId") int customerId)
			throws SpendAnalyzerException {

		LoggerUtil.info(SpendAnalyzerConstants.ENTRY + TransactionControllerImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		List<Transaction> transactionList = new ArrayList<>();
		ResponseDto<Object> response = new ResponseDto<>();

		// Get the list of transaction for specific customer
		transactionList = transService.getTransactionList(customerId);

		LoggerUtil.debug(SpendAnalyzerConstants.TRANSACTION_SIZE_MESSAGE + transactionList.size());
		response.setStatus(1);
		response.setData(transactionList);
		LoggerUtil.info(SpendAnalyzerConstants.EXIT + TransactionControllerImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	@SuppressWarnings("rawtypes")
	@Override
	@PostMapping(value = "/{customerId}/makepayment")
	public ResponseEntity<ResponseDto> makePayment(@Valid @RequestBody PaymentDto paymentDto,
			BindingResult bindingResult) throws SpendAnalyzerException {

		LoggerUtil.info(SpendAnalyzerConstants.ENTRY + TransactionControllerImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		ResponseDto<TransactionDto> response = new ResponseDto<>();

	
		List<Error> errorList = new ArrayList<>();

		// Catch all the errors occur at model level
		if (bindingResult.hasErrors()) {
			List<FieldError> errors = bindingResult.getFieldErrors();
			String message;

			for (FieldError errorVal : errors) {
				Error error = new Error();
				message = errorVal.getField().toUpperCase() + " : " + errorVal.getDefaultMessage();
				error.setErrorCode(HttpStatus.BAD_REQUEST.toString());
				error.setErrorMessage(message);
				errorList.add(error);
			}
			throw new SpendAnalyzerException(errorList, HttpStatus.OK.toString());
		}
		response.setData(transService.makePayment(paymentDto));
		response.setStatus(1);
		LoggerUtil.info(SpendAnalyzerConstants.EXIT + TransactionControllerImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	@Override
	@GetMapping(value = "/{customerId}/transactionsummary")
	public ResponseDto<List<TransactionSummaryDto>> getTransactionSummary(@PathVariable("customerId") int customerId)
			throws SpendAnalyzerException {

		LoggerUtil.info(SpendAnalyzerConstants.ENTRY + TransactionControllerImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());

		ResponseDto<List<TransactionSummaryDto>> response = new ResponseDto<>();

		response.setData(transService.getTransactionSummary(customerId));
		response.setStatus(1);

		LoggerUtil.info(SpendAnalyzerConstants.EXIT + TransactionControllerImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		return response;
	}

}
