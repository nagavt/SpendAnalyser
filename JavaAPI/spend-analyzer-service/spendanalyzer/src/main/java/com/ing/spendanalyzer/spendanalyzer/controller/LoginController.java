package com.ing.spendanalyzer.spendanalyzer.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;
import com.ing.spendanalyzer.spendanalyzer.model.Customer;

/**
 * 
 * @author rameswari
 *
 */
public interface LoginController  {

	/**
	 * loginUser -- This method validates the values passed and 
	 * calls the appropriate method in service layer to check the login details 
	 * 
	 * @param customerDto
	 * @return ResponseDto object along with HTTP status code
	 * @throws Exception
	 */
	public ResponseEntity<Object> loginUser(Customer customerDto,BindingResult bindingResult) throws SpendAnalyzerException;
}
