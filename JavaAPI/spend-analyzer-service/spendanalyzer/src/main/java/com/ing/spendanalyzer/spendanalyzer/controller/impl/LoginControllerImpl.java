package com.ing.spendanalyzer.spendanalyzer.controller.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ing.spendanalyzer.spendanalyzer.constants.SpendAnalyzerConstants;
import com.ing.spendanalyzer.spendanalyzer.controller.LoginController;
import com.ing.spendanalyzer.spendanalyzer.dto.CustomerDto;
import com.ing.spendanalyzer.spendanalyzer.dto.ResponseDto;
import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;
import com.ing.spendanalyzer.spendanalyzer.model.Customer;
import com.ing.spendanalyzer.spendanalyzer.model.Error;
import com.ing.spendanalyzer.spendanalyzer.service.LoginService;
import com.ing.spendanalyzer.spendanalyzer.util.LoggerUtil;

/**
 * @author pallabothu, rameshwari
 * @version
 * @since
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class LoginControllerImpl implements LoginController {

	@Autowired
	private LoginService loginService;

	@PostMapping(value = "/login")
	public ResponseEntity<Object> loginUser(@Valid @RequestBody Customer customerReq, BindingResult bindingResult)
			throws SpendAnalyzerException {

		LoggerUtil.info(SpendAnalyzerConstants.ENTRY + LoginControllerImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		Customer customer;
		CustomerDto response = new CustomerDto();
		Error error;
		ResponseDto<Object> responseDto = new ResponseDto<>();
		List<Error> errorList = new ArrayList<>();

		// Catch all the errors occur at model level
		if (bindingResult.hasErrors()) {
			List<FieldError> errors = bindingResult.getFieldErrors();
			String message;

			for (FieldError errorVal : errors) {
				error = new Error();
				message = errorVal.getField().toUpperCase() + " : " + errorVal.getDefaultMessage();
				error.setErrorCode(HttpStatus.BAD_REQUEST.toString());
				error.setErrorMessage(message);
				errorList.add(error);

			}
			throw new SpendAnalyzerException(errorList, HttpStatus.OK.toString());

		}

		customer = loginService.findByLoginId(customerReq.getLoginId());

		LoggerUtil.debug(SpendAnalyzerConstants.CUSTOMER_LOGIN_ID + customer.getLoginId());
		if (customer.getPassword().equals(customerReq.getPassword())) {
			response.setCustomerId(customer.getCustomerId());
			response.setLoginId(customer.getLoginId());
			response.setName(customer.getName());
			responseDto.setData(response);
			responseDto.setStatus(1);

		} else {
			error = new Error(HttpStatus.BAD_REQUEST.toString(), SpendAnalyzerConstants.ERROR_MESSAGE_USERNAME_PASSWORD_NOTVALID);
			errorList.add(error);
			throw new SpendAnalyzerException(errorList, HttpStatus.OK.toString());
		}
		LoggerUtil.info(SpendAnalyzerConstants.EXIT + LoginControllerImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		return new ResponseEntity<>(responseDto, HttpStatus.OK);

	}

}
