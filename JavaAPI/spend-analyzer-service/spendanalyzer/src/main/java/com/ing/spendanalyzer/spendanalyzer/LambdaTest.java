package com.ing.spendanalyzer.spendanalyzer;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import com.ing.spendanalyzer.spendanalyzer.enums.PaymentType;
import com.ing.spendanalyzer.spendanalyzer.model.Transaction;

public class LambdaTest {
public static void main(String[] args) {
	
	SimpleDateFormat sf = new SimpleDateFormat("MMM");
	Transaction t1 = new Transaction();
	t1.setTransactionId(1);
	t1.setTransactionDate(new Date() );
	t1.setPaymentType("CREDIT");
	t1.setTransactionAmount(500.0);
	t1.setClosingBalance(500.0);
	t1.setDescription("ABC");
	
	Transaction t2 = new Transaction();
	Calendar cal = Calendar.getInstance();
	cal.set(Calendar.MONTH, 4);
	t2.setTransactionId(2);
	t2.setTransactionDate(cal.getTime());
	t2.setPaymentType("DEBIT");
	t2.setTransactionAmount(60.0);
	t2.setClosingBalance(100.0);
	t2.setDescription("ABC");
	
	
	Transaction t3 = new Transaction();
	
	t3.setTransactionId(3);
	t3.setTransactionDate(cal.getTime());
	t3.setPaymentType("CREDIT");
	t3.setTransactionAmount(500.0);
	t3.setClosingBalance(500.0);
	t3.setDescription("ABC");
	
	
	Transaction t4 = new Transaction();
	t4.setTransactionId(4);
	t4.setTransactionDate(new Date());
	t4.setPaymentType("CREDIT");
	t4.setTransactionAmount(40.0);
	t4.setClosingBalance(250.0);
	t4.setDescription("ABC");
	
	
	Transaction t5 = new Transaction();
	cal.set(Calendar.MONTH, 4);
	t5.setTransactionId(5);
	t5.setTransactionDate(cal.getTime());
	t5.setPaymentType("DEBIT");
	t5.setTransactionAmount(200.0);
	t5.setClosingBalance(150.0);
	t5.setDescription("ABC");
	
	List<Transaction> list = new ArrayList<Transaction>();
	list.add(t1);
	list.add(t2);
	list.add(t3);
	list.add(t4);
	list.add(t5);
/*	Map<Object, List<Transaction>> counting = list.stream().collect(
            Collectors.groupingBy(p -> sf.format(p.getTransactionDate()), Collectors.toList()));*/
	
	Map<String, Map<String, Double>> summaryMap = list.stream().collect(
            Collectors.groupingBy(p -> sf.format(p.getTransactionDate()), Collectors.groupingBy(p -> p.getPaymentType(),Collectors.summingDouble(p -> p.getTransactionAmount()))));
	
	
	Map<String, Optional<Transaction>> closingBalanceMap = list.stream().collect(
            Collectors.groupingBy(p -> sf.format(p.getTransactionDate()), Collectors.maxBy(new TransactionComparator())));
	
	
	
	for(Entry<String, Map<String, Double>> entry : summaryMap.entrySet()){
		
		String month = entry.getKey();
		double closingBal = 0;
		if(closingBalanceMap.containsKey(month)){
		 closingBal = closingBalanceMap.get(month).get().getClosingBalance();
		}
		
		Map<String, Double> inOutMap = entry.getValue();
		double totalIncoming = 0;
		if(inOutMap.containsKey(PaymentType.CREDIT.getPaymentTypeValue())){
			totalIncoming = inOutMap.get(PaymentType.CREDIT.getPaymentTypeValue());
		}
		double totalOutgoing = 0;
		if(inOutMap.containsKey(PaymentType.DEBIT.getPaymentTypeValue())){
			totalOutgoing = inOutMap.get(PaymentType.DEBIT.getPaymentTypeValue());
		}
		
		

		System.out.println("Transaction for "+ month + " :: Total Incoming = "+totalIncoming+" Total outgoing = "+totalOutgoing + " Closing Balance = "+closingBal);
	}
	
	/*Optional<Transaction> maxTransaction = closingBalanceMap.get("Jul");
	maxTransaction.get().getClosingBalance();
	
		System.out.println(maxTransaction.get().getClosingBalance());*/

		//System.out.println(closingBalanceMap);
	
	
}
}

class TransactionComparator implements Comparator<Transaction>{
	@Override
	public int compare(Transaction o1, Transaction o2) {
		// TODO Auto-generated method stub
		return o1.getTransactionId()-o2.getTransactionId();
	}
}
