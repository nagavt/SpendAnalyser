package com.ing.spendanalyzer.spendanalyzer.exception;

import java.io.Serializable;
import java.util.List;

import com.ing.spendanalyzer.spendanalyzer.model.Error;

public class SpendAnalyzerException extends Exception implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private List<Error> errors;
	private String statusCode;

	public SpendAnalyzerException(String message) {
		super(message);
	}

	public SpendAnalyzerException(String message,Exception exp) {
		super(message,exp);
	}
	
	public SpendAnalyzerException(List<Error> errors, String statusCode) {
		super();
		this.errors = errors;
		this.statusCode = statusCode;
	}

	public List<Error> getErrors() {
		return errors;
	}

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public String toString() {
		return "SpendAnalyzerException [errors=" + errors + ", statusCode=" + statusCode + "]";
	}


	
	
}