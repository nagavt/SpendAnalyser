package com.ing.spendanalyzer.spendanalyzer.service;

import java.util.List;

import com.ing.spendanalyzer.spendanalyzer.dto.PaymentDto;
import com.ing.spendanalyzer.spendanalyzer.dto.TransactionDto;
import com.ing.spendanalyzer.spendanalyzer.dto.TransactionSummaryDto;
import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;
import com.ing.spendanalyzer.spendanalyzer.model.Transaction;

public interface TransactionService {

	List<Transaction> getTransactionList(int customerId) throws SpendAnalyzerException;
	TransactionDto makePayment(PaymentDto paymentDto) throws SpendAnalyzerException;
	List<TransactionSummaryDto> getTransactionSummary(int customerId)throws SpendAnalyzerException;

}
