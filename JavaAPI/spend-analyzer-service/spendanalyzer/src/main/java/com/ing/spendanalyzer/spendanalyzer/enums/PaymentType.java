package com.ing.spendanalyzer.spendanalyzer.enums;
/**
 * 
 * @author srikanth
 *
 */
public enum PaymentType {

	CREDIT("CREDIT"), DEBIT("DEBIT");
	private String paymentTypeValue;

	public String getPaymentTypeValue() {
		return paymentTypeValue;
	}

	private PaymentType(String paymentTypeValue) {
		this.paymentTypeValue = paymentTypeValue;
	}
	
}
