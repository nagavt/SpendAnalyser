package com.ing.spendanalyzer.spendanalyzer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ing.spendanalyzer.spendanalyzer.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>{

	Account findByCustomerId(Integer customerId);
	
	

}
