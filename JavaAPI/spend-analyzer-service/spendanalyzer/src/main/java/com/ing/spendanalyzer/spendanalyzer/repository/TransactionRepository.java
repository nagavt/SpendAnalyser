package com.ing.spendanalyzer.spendanalyzer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ing.spendanalyzer.spendanalyzer.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer>{

	
	
	@Query("select transaction from Transaction transaction where transaction.accountId=:accountId order by transaction.transactionDate desc")
	List<Transaction> findByAccountId(@Param("accountId")Integer accountId);

	@Query(value= "select transaction from Transaction transaction where account_id = :accountId ")
	List<Transaction> getTransactionSummary(@Param("accountId") int accountId);
	

}
