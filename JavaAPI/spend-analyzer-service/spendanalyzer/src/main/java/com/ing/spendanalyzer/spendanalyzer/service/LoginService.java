package com.ing.spendanalyzer.spendanalyzer.service;

import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;
import com.ing.spendanalyzer.spendanalyzer.model.Customer;

public interface LoginService {

	/**
	 * loginUser -- Checks for login details entered by the user and returns if exists in the database
	 * 
	 * @param customerRequest
	 * @return Customer object
	 * 
	 */
	public Customer findByLoginId(String loginId) throws SpendAnalyzerException;

}
