package com.ing.spendanalyzer.spendanalyzer.exception;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ing.spendanalyzer.spendanalyzer.dto.ErrorResponse;
import com.ing.spendanalyzer.spendanalyzer.model.Error;
import com.ing.spendanalyzer.spendanalyzer.model.Errors;
/**
 * 
 * @author rameswari, bala
 *
 */
@ControllerAdvice
public class GlobalExceptionController extends ResponseEntityExceptionHandler{

	public static final Logger logger = Logger.getLogger(GlobalExceptionController.class);	

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(
			MissingServletRequestParameterException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		Errors errors = new Errors();			
		Error error = new Error(HttpStatus.BAD_REQUEST.toString(),ex.getLocalizedMessage());
		errors.add(error);
		return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		BindingResult result = ex.getBindingResult();        
		Errors errorObject = processFieldErrors(result.getFieldErrors(), status.toString());			

		return new ResponseEntity<>(errorObject, headers, status);
	}	

	public Errors processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors , String errorCode){
		Errors errorObject=new Errors();
		for(org.springframework.validation.FieldError fieldError : fieldErrors){
			errorObject.add(new Error(errorCode,fieldError.getDefaultMessage()));
		}		
		return errorObject;

	}
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ErrorResponse> handleAllExceptions(Exception ex, WebRequest request) {
		List<Error> errorList = new ArrayList<>();
		errorList.add(new Error(HttpStatus.INTERNAL_SERVER_ERROR.toString(), ex.getMessage()));
		ErrorResponse errorDetails = new ErrorResponse(0, errorList);
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(SpendAnalyzerException.class)
	public final ResponseEntity<ErrorResponse> handleSpendAnalyzerException(SpendAnalyzerException ex, WebRequest request) {
		ErrorResponse errorDetails = new ErrorResponse(0,ex.getErrors());
		String statusCode = ex.getStatusCode();
		if(statusCode.equals("404"))
			return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
		else if(statusCode.equals("400"))
			return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
		else if(statusCode.equals("401"))
			return new ResponseEntity<>(errorDetails, HttpStatus.UNAUTHORIZED);
		else if(statusCode.equals("200"))
			return new ResponseEntity<>(errorDetails, HttpStatus.OK);
		else 
			return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}


