package com.ing.spendanalyzer.spendanalyzer.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TransactionDto {

	 private Integer transactionId;
	 
	 @JsonFormat(pattern="DD-MM-YYYY hh:mm:ss")
	 private Date transactionDate;
	 private String description;
	 private Double amount;
	 private String paymentType;
	 private Double closingBalance;
	 
	public Integer getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Double getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(Double closingBalance) {
		this.closingBalance = closingBalance;
	}
	@Override
	public String toString() {
		return "TransactionDto [transactionId=" + transactionId + ", transactionDate=" + transactionDate
				+ ", description=" + description + ", amount=" + amount + ", paymentType=" + paymentType
				+ ", closingBalance=" + closingBalance + "]";
	}
	 
	 
}
