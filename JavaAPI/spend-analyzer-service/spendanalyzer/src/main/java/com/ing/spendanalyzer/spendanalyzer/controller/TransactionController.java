package com.ing.spendanalyzer.spendanalyzer.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import com.ing.spendanalyzer.spendanalyzer.dto.PaymentDto;
import com.ing.spendanalyzer.spendanalyzer.dto.ResponseDto;
import com.ing.spendanalyzer.spendanalyzer.dto.TransactionSummaryDto;
import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;

/**
 * 
 * @author rameswari
 *
 */
public interface TransactionController {

	/**
	 * fetch the transactions from the repository
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ResponseEntity<ResponseDto> getTransactions(int customerId) throws Exception;
	
	/**
	 * makes a payment 
	 * @param PaymentDto
	 * @return
	 * @throws SpendAnalyzerException
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ResponseEntity<ResponseDto> makePayment(PaymentDto paymentDto, BindingResult bindingResult) throws SpendAnalyzerException;
	
	public ResponseDto<List<TransactionSummaryDto>> getTransactionSummary(int customerId) throws SpendAnalyzerException;

}
