package com.ing.spendanalyzer.spendanalyzer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="TRANSACTION")
public class Transaction implements Serializable {
	
	private static final long serialVersionUID= 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="transaction_id")
	private Integer transactionId;
	
	@Column(name="payment_type")
	private String paymentType;
	
	@Column(name="transaction_amount")
	private Double transactionAmount;
	
	@Column(name="transaction_date")
	private Date transactionDate;
	
	@Column(name="description")
	private String description;
	
	@Column(name="closing_balance")
	private Double closingBalance;
	
	
	@Column(name="account_id")
	private Integer accountId;
	
	/*@ManyToOne
	@JoinColumn(name="account_id",referencedColumnName="account_id")
	private Account account;*/
	
	
	public Integer getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm", timezone="IST")
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(Double closingBalance) {
		this.closingBalance = closingBalance;
	}
	
	
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	
	@Override
	public String toString() {
		return "Transaction [transactionId=" + transactionId + ", paymentType=" + paymentType + ", transactionAmount="
				+ transactionAmount + ", transactionDate=" + transactionDate + ", description=" + description
				+ ", closingBalance=" + closingBalance + ", accountId=" + accountId + "]";
	}
	
	
	
	
	
	
}
