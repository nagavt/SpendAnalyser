package com.ing.spendanalyzer.spendanalyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author pallabothu
 * @since 22-07-2018
 * @version 1.0
 *
 */
@SpringBootApplication
public class SpendanalyzerApplication {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpendanalyzerApplication.class, args);
	}
}
