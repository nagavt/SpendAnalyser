package com.ing.spendanalyzer.spendanalyzer.dto;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PaymentDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull(message="{custId.notBlank}")
	private Integer customerId;
	private String description;
	@NotBlank(message="{paymentType.value}")
	private String paymentType;
	@NotNull(message="{amount.value}")
	@Min(value=0,message="{amount.positive.value}")
	private Double amount;
	
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "PaymentDto [customerId=" + customerId + ", description=" + description + ", paymentType=" + paymentType
				+ ", amount=" + amount + "]";
	}

	
}
