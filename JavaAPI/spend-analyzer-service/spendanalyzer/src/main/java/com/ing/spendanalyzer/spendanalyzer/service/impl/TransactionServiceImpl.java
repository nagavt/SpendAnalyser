package com.ing.spendanalyzer.spendanalyzer.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ing.spendanalyzer.spendanalyzer.constants.SpendAnalyzerConstants;
import com.ing.spendanalyzer.spendanalyzer.dto.PaymentDto;
import com.ing.spendanalyzer.spendanalyzer.dto.TransactionDto;
import com.ing.spendanalyzer.spendanalyzer.dto.TransactionSummaryDto;
import com.ing.spendanalyzer.spendanalyzer.enums.PaymentType;
import com.ing.spendanalyzer.spendanalyzer.exception.SpendAnalyzerException;
import com.ing.spendanalyzer.spendanalyzer.model.Account;
import com.ing.spendanalyzer.spendanalyzer.model.Error;
import com.ing.spendanalyzer.spendanalyzer.model.Transaction;
import com.ing.spendanalyzer.spendanalyzer.repository.AccountRepository;
import com.ing.spendanalyzer.spendanalyzer.repository.TransactionRepository;
import com.ing.spendanalyzer.spendanalyzer.service.TransactionService;
import com.ing.spendanalyzer.spendanalyzer.util.LoggerUtil;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private AccountRepository accountRepository;

	private SimpleDateFormat sf = new SimpleDateFormat("MMM - yyyy");

	@Override
	public List<Transaction> getTransactionList(int customerId) throws SpendAnalyzerException {
		LoggerUtil.info(SpendAnalyzerConstants.ENTRY + TransactionServiceImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		List<Transaction> transactionList = new ArrayList<>();
		List<Error> errorList = new ArrayList<>();

		Account account = null;

		try {

			/*
			 * if (!StringUtils.isEmpty(customerId)) { account =
			 * accountRepository.findByCustomerId(customerId); }
			 */

			account = accountRepository.findByCustomerId(customerId);
			if (account == null) {
				LoggerUtil.error(SpendAnalyzerConstants.CUSTOMER_OR_ACCOUNT_NOT_FOUND + customerId);
				Error error = new Error(HttpStatus.NOT_FOUND.toString(),
						SpendAnalyzerConstants.CUSTOMER_OR_ACCOUNT_NOT_FOUND + customerId);
				errorList.add(error);
				throw new SpendAnalyzerException(errorList, HttpStatus.OK.toString());
			}
			transactionList = transactionRepository.findByAccountId(account.getAccountId());
			LoggerUtil.info(SpendAnalyzerConstants.TRANSACTION_MESSAGE + customerId + SpendAnalyzerConstants.EMPTY
					+ transactionList);

			if (transactionList.isEmpty()) {
				LoggerUtil.error(SpendAnalyzerConstants.NO_TRANSACTION_LIST + customerId);
				Error error = new Error(HttpStatus.NOT_FOUND.toString(),
						SpendAnalyzerConstants.NO_TRANSACTION_LIST + customerId);
				errorList.add(error);
				throw new SpendAnalyzerException(errorList, HttpStatus.OK.toString());

			}

		} catch (DataAccessException exp) {
			LoggerUtil.error(SpendAnalyzerConstants.DATABASE_ERROR_FETCH + exp);
			Error error = new Error(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
					SpendAnalyzerConstants.DATABASE_ERROR_FETCH + customerId);
			errorList.add(error);
			throw new SpendAnalyzerException(errorList, HttpStatus.INTERNAL_SERVER_ERROR.toString());
		}

		LoggerUtil.info(SpendAnalyzerConstants.EXIT + TransactionServiceImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());

		return transactionList;
	}

	@Override
	public TransactionDto makePayment(PaymentDto paymentDto) throws SpendAnalyzerException {
		LoggerUtil.info(SpendAnalyzerConstants.ENTRY + TransactionServiceImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		LoggerUtil.info(SpendAnalyzerConstants.PAYMENT_PROCESS_MESSAGE + paymentDto.getCustomerId());
		TransactionDto transactionDto = null;
		List<Error> errorList = new ArrayList<>();
		try {

			Account account = accountRepository.findByCustomerId(paymentDto.getCustomerId());
			LoggerUtil.info(Thread.currentThread().getStackTrace()[1].getMethodName()
					+ SpendAnalyzerConstants.ACCOUNT_INFO_MESSAGE + account);

			ModelMapper mapper = new ModelMapper();

			if (account == null) {
				LoggerUtil.error(SpendAnalyzerConstants.CUSTOMER_OR_ACCOUNT_NOT_FOUND + paymentDto.getCustomerId());
				Error error = new Error(HttpStatus.NOT_FOUND.toString(),
						SpendAnalyzerConstants.CUSTOMER_OR_ACCOUNT_NOT_FOUND + paymentDto.getCustomerId()
								+ SpendAnalyzerConstants.TRANSACTION_FAILURE);
				errorList.add(error);
				throw new SpendAnalyzerException(errorList, HttpStatus.BAD_REQUEST.toString());
			}

			Transaction transaction = new Transaction();
			transaction.setAccountId(account.getAccountId());
			transaction.setDescription(paymentDto.getDescription());
			transaction.setPaymentType(paymentDto.getPaymentType().toUpperCase());
			transaction.setTransactionAmount(paymentDto.getAmount());
			transaction.setTransactionDate(new Date());

			if (PaymentType.CREDIT.getPaymentTypeValue().equals(paymentDto.getPaymentType().toUpperCase())) {
				account.setCurrentBalance(account.getCurrentBalance() + paymentDto.getAmount());
			} else if (PaymentType.DEBIT.getPaymentTypeValue().equals(paymentDto.getPaymentType().toUpperCase())) {
				account.setCurrentBalance(account.getCurrentBalance() - paymentDto.getAmount());
				if (account.getCurrentBalance() < 0) {
					Error error = new Error(HttpStatus.BAD_REQUEST.toString(),
							SpendAnalyzerConstants.INSUFFICIENT_BALANCE);
					errorList.add(error);
					throw new SpendAnalyzerException(errorList, HttpStatus.OK.toString());
				}
			}
			transaction.setClosingBalance(account.getCurrentBalance());

			accountRepository.save(account);
			transactionRepository.save(transaction);

			transactionDto = mapper.map(transaction, TransactionDto.class);

		} catch (DataAccessException exp) {
			LoggerUtil.error(Thread.currentThread().getStackTrace()[1].getMethodName()
					+ SpendAnalyzerConstants.DATABASE_ERROR_PAYMENT + exp);
			Error error = new Error(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
					SpendAnalyzerConstants.DATABASE_ERROR_PAYMENT + exp.getMessage());
			errorList.add(error);
			throw new SpendAnalyzerException(errorList, HttpStatus.INTERNAL_SERVER_ERROR.toString());
		}

		LoggerUtil.info(SpendAnalyzerConstants.EXIT + TransactionServiceImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());

		return transactionDto;
	}

	@Override
	public List<TransactionSummaryDto> getTransactionSummary(int customerId) throws SpendAnalyzerException {

		LoggerUtil.info(SpendAnalyzerConstants.ENTRY + TransactionServiceImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());

		List<TransactionSummaryDto> summaryList = new ArrayList<>();
		List<Transaction> transactions = new ArrayList<>();
		List<Error> errorList = new ArrayList<>();

		try {
			Account account = accountRepository.findByCustomerId(customerId);

			if (account == null) {
				LoggerUtil.error(Thread.currentThread().getStackTrace()[1].getMethodName()
						+ SpendAnalyzerConstants.CUSTOMER_OR_ACCOUNT_NOT_FOUND + customerId);
				Error error = new Error(HttpStatus.NOT_FOUND.toString(),
						SpendAnalyzerConstants.CUSTOMER_OR_ACCOUNT_NOT_FOUND + customerId);
				errorList.add(error);
				throw new SpendAnalyzerException(errorList, HttpStatus.OK.toString());
			}

			transactions = transactionRepository.getTransactionSummary(account.getAccountId());

			if (transactions == null || transactions.isEmpty()) {
				LoggerUtil.error(Thread.currentThread().getStackTrace()[1].getMethodName()
						+ SpendAnalyzerConstants.NO_TRANSACTION_FOUND + customerId);
				Error error = new Error(HttpStatus.NOT_FOUND.toString(),
						SpendAnalyzerConstants.NO_TRANSACTION_FOUND + customerId);
				errorList.add(error);
				throw new SpendAnalyzerException(errorList, HttpStatus.OK.toString());
			}

			Map<String, Map<String, Double>> summaryMap = transactions.stream()
					.collect(Collectors.groupingBy(p -> sf.format(p.getTransactionDate()), Collectors.groupingBy(
							p -> p.getPaymentType(), Collectors.summingDouble(p -> p.getTransactionAmount()))));

			Map<String, Optional<Transaction>> closingBalanceMap = transactions.stream().collect(Collectors
					.groupingBy(p -> sf.format(p.getTransactionDate()), Collectors.maxBy(new TransactionComparator())));

			for (Entry<String, Map<String, Double>> entry : summaryMap.entrySet()) {

				TransactionSummaryDto transactionSummaryDto = new TransactionSummaryDto();
				String month = entry.getKey();
				double closingBal = 0.00;
				if (closingBalanceMap.containsKey(month)) {
					closingBal = closingBalanceMap.get(month).get().getClosingBalance();
				}

				Map<String, Double> inOutMap = entry.getValue();
				double totalIncoming = 0.00;
				if (inOutMap.containsKey(PaymentType.CREDIT.getPaymentTypeValue())) {
					totalIncoming = inOutMap.get(PaymentType.CREDIT.getPaymentTypeValue());
				}
				double totalOutgoing = 0.00;
				if (inOutMap.containsKey(PaymentType.DEBIT.getPaymentTypeValue())) {
					totalOutgoing = inOutMap.get(PaymentType.DEBIT.getPaymentTypeValue());
				}

				transactionSummaryDto.setClosingBalance(closingBal);
				transactionSummaryDto.setMonth(month);
				transactionSummaryDto.setTotalIncoming(totalIncoming);
				transactionSummaryDto.setTotalOutgoing(totalOutgoing);
				transactionSummaryDto.setLoginId(customerId + "");

				LoggerUtil.info(Thread.currentThread().getStackTrace()[1].getMethodName()
						+ SpendAnalyzerConstants.CUSTOMER_TRANSACTION_SUMMARY + customerId 
						+ summaryList.size());
				summaryList.add(transactionSummaryDto);

			}
		} catch (DataAccessException exp) {
			LoggerUtil.error(SpendAnalyzerConstants.DATABASE_ERROR_FETCH + exp);
			Error error = new Error(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
					SpendAnalyzerConstants.DATABASE_ERROR_FETCH + exp.getMessage());
			errorList.add(error);
			throw new SpendAnalyzerException(errorList, HttpStatus.INTERNAL_SERVER_ERROR.toString());
		}
		LoggerUtil.info(SpendAnalyzerConstants.EXIT + TransactionServiceImpl.class.getName()
				+ SpendAnalyzerConstants.METHOD_NAME + Thread.currentThread().getStackTrace()[1].getMethodName());
		return summaryList;
	}
}

class TransactionComparator implements Comparator<Transaction> {
	@Override
	public int compare(Transaction o1, Transaction o2) {
		return o1.getTransactionId() - o2.getTransactionId();
	}
}
