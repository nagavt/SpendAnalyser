package com.ing.spendanalyzer.spendanalyzer.constants;

public interface SpendAnalyzerConstants {
	String ENTRY = "ENTRY ";
	String EXIT = "EXIT ";
	
	String EMPTY = " ";
	String METHOD_NAME = " METHODNAME : ";
	String ERROR_MESSAGE_USERNAME_PASSWORD_NOTVALID= "Please enter valid username and password";
	String CUSTOMER_LOGIN_ID= "CUSTOMER LOGIN ID: ";
	String TRANSACTION_SIZE_MESSAGE="Size of transaction list: ";
	String TRANSACTION_MESSAGE="List of transactions got for the customer: ";
	String NO_TRANSACTION_LIST="No transactions found for the customer:";
	String TRANSACTION_FAILURE=" Could not make a transaction.";
	String CUSTOMER_NOT_FOUND_MESSAGE="No record found for customer: ";
	String CUSTOMER_OR_ACCOUNT_NOT_FOUND = "No account/customer details found for the given customer id: ";
	String CUSTOMER_NO_ACCOUNT="No account exist for the customer:";
	String DATABASE_ERROR_FETCH="Data Base Error occurred while fetching transaction List, Exception: ";
	String TRANSACTION_LIST_FETCH_ERROR="Error occurred while fetching transaction List ";
	String INSUFFICIENT_BALANCE="Insufficient balance in account to do debit operation.";
	String DATABASE_ERROR_PAYMENT="Data Base Error occurred while make payment, Exception: ";
	String PAYMENT_ERROR="Error occurred while make payment ";
	String NO_TRANSACTION_FOUND="No Transactions found for the given period for the customer ID: ";
	String PAYMENT_PROCESS_MESSAGE="Processing payment for the customer: ";
	String ACCOUNT_INFO_MESSAGE=" Got Account Info";
	String CUSTOMER_TRANSACTION_SUMMARY=": Got transaction summary for the customer:";
	
}
