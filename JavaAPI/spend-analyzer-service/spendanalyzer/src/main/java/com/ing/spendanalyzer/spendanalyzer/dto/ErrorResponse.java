package com.ing.spendanalyzer.spendanalyzer.dto;

import java.util.List;

import com.ing.spendanalyzer.spendanalyzer.model.Error;

public class ErrorResponse {


	private List<Error> errors;
	private int status;
	
	public ErrorResponse(int status,List<Error> errors) {
		super();
		this.status = status;
		this.errors = errors;
		
	}

	public List<Error> getErrors() {
		return errors;
	}

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ErrorResponse [errors=" + errors + ", status=" + status + "]";
	}


}
