package com.ing.spendanalyzer.spendanalyzer.dto;

public class TransactionSummaryDto {

	private String month;
	private String loginId;
	private Double totalIncoming;
	private Double totalOutgoing;
	private Double closingBalance;
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}

	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Double getTotalIncoming() {
		return totalIncoming;
	}
	public void setTotalIncoming(Double totalIncoming) {
		this.totalIncoming = totalIncoming;
	}
	public Double getTotalOutgoing() {
		return totalOutgoing;
	}
	public void setTotalOutgoing(Double totalOutgoing) {
		this.totalOutgoing = totalOutgoing;
	}
	public Double getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(Double closingBalance) {
		this.closingBalance = closingBalance;
	}


}
