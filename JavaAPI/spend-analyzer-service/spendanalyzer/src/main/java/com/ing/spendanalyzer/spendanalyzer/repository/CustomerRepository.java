package com.ing.spendanalyzer.spendanalyzer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ing.spendanalyzer.spendanalyzer.model.Customer;



public interface CustomerRepository extends JpaRepository<Customer, Integer>{

	
	Customer findByLoginId(String loginId);
	


}
