package com.ing.spendanalyzer.spendanalyzer.util;

import java.util.Date;

public class LoggerUtil {
	
	
	/**
     * DEBUG will show all of the logs, use when want to see logs of from 
     */
    public static final LogLevel DEBUG = LogLevel.DEBUG;
     
    /**
     * INFO will show logs of INFO and ERROR
     */
    public static final LogLevel INFO = LogLevel.INFO;
     
    /**
     * ERROR will show only of ERROR only
     */
    public static final LogLevel ERROR = LogLevel.ERROR;//Lowest Value of the Log- Minimum Trace
     
   static Date logDateTime = new Date();
 
    /*
     * Default Log Level
     */
    private static int intDefaultlog = -1;
    
    private static void log(LogLevel level, String msg) {
        if ( (level.ordinal() > ERROR.ordinal()) || (level.ordinal() < DEBUG.ordinal()) )
            return; 
        if (level.ordinal() >= intDefaultlog) {
            logDateTime.setTime(System.currentTimeMillis());
            System.out.println(logDateTime + " :: #"+level.name()+"## : " + msg);
        }
        return;
    }
     
    /**
     * this method will print logs only when logger value is set to ERROR
     * @param msg the logging message to print
     */
    public static void error(String msg){
        log(LogLevel.ERROR, msg);
    }
     
    /**
     * this method will print logs only when logger value is set to INFO / ERROR
     * @param msg the logging message to print
     */
    public static void info(String msg){
        log(LogLevel.INFO, msg);
    }
     
    /**
     * this method will print logs for every level
     * @param msg the logging message to print
     */
    public static void debug(String msg){
        log(LogLevel.DEBUG, msg);
    }
 
    /**
     * this method is default logging method, work as {@link #info(String)} method. Suggested to ignore this and use other logging methods for better readability. 
     */
    public static void log(String msg){
        log(LogLevel.INFO, msg);
    }   
     
}
enum LogLevel{
    DEBUG,
    INFO,
    ERROR;
}
