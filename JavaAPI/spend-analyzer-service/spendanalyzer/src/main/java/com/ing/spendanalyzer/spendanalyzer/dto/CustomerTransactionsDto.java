package com.ing.spendanalyzer.spendanalyzer.dto;

import java.util.List;

public class CustomerTransactionsDto {

	List<TransactionRequestDto> transactions;

	public List<TransactionRequestDto> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionRequestDto> transactions) {
		this.transactions = transactions;
	}
	
	
}
