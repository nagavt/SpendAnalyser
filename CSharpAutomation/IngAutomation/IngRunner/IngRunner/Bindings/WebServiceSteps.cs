﻿using System;
using TechTalk.SpecFlow;
using IngFundAnalyzerTestFramework.CommonModules;
using System.Net;
using IngFundAnalyzerTestFramework.Fixures;

namespace IngRunner.Bindings
{
    [Binding]
    public class WebServiceSteps
    {
        RestClient restclient = new RestClient();
        

        [Given(@"I have Login API Available")]
        public void GivenIHaveLoginAPIAvailable()
        {
            restclient.EndPoint = $"{Configs.ApiUrl}{Configs.LoginApiParams}";
            restclient.Method = Verb.POST;
            var pdata = restclient.PostData;
        }

        [Given(@"I have Payment API Available")]
        public void GivenIHavePaymentAPIAvailable()
        {
            restclient.EndPoint = $"{Configs.ApiUrl}{Configs.PaymentApiParams}";
            restclient.Method = Verb.POST;
            var pdata = restclient.PostData;
        }


        [When(@"I call Login API with Post Data using '(.*)' '(.*)'")]
        public void WhenICallRestAPIWithJsonPopulatedFrom(string userName, string password)
        {
            string loginResponseFileName=restclient.BuildLoginJson(userName, password);
            restclient.PostLoginAPI(restclient.EndPoint);
        }

        [When(@"I call Payment API with Post Data using (.*) '(.*)' '(.*)' '(.*)'")]
        public void WhenICallPaymentAPIWithPostDataUsing(int custId, int amount, string paymentType, string description)
        {
            string jsonFileName = restclient.BuildPaymentJson(custId, amount,paymentType,description);
            restclient.PostPaymentAPI(restclient.EndPoint);
        }


        [Then(@"Login Response should be successful")]
        public void ThenServerResponseShouldBeSuccessful()
        {
            Console.WriteLine($"API Response code is: {restclient.responseCode}");
            if (restclient.responseCode != HttpStatusCode.OK)
            {
                Console.WriteLine($"Response Code is {restclient.responseCode}");
                throw new Exception("Rest request is not successful");
            }
            restclient.ReadLoginResponse();
        }

        [Then(@"Payment Response should be successful")]
        public void ThenPaymentResponseShouldBeSuccessful()
        {
            Console.WriteLine($"API Response code is: {restclient.responseCode}");
            if (restclient.responseCode != HttpStatusCode.OK)
            {
                Console.WriteLine($"Response Code is {restclient.responseCode}");
                throw new Exception("Rest request is not successful");
            }
            restclient.ReadPaymentResponse();
        }


        [Then(@"Login response Status should contains status '(.*)' and Name '(.*)'")]
        public void ThenServerResponseStatusShouldContainsStatusAndName(string statucCode, string name)
        {
            Console.WriteLine($"API Response code is: {restclient.responseCode}");
            if (restclient.responseCode != HttpStatusCode.OK)
            {
                Console.WriteLine($"Response Code is {restclient.responseCode}");
                throw new Exception("Rest request is not successful");
            }
            restclient.ValidateLoginResponse(statucCode,name);
        }

    }
}
