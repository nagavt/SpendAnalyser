﻿using System;
using IngFundAnalyzerTestFramework.Pages;
using IngFundAnalyzerTestFramework.CommonModules;
using TechTalk.SpecFlow;

namespace IngRunner.Bindings
{
    [Binding]
    public class Hooks
    {
        [BeforeFeature, Scope(Feature = "FinanceSpendAnalyzer")]
        public static void BeforeFeatureSteps()
        {
            Driver.BrowserSetup();
        }

        [AfterFeature, Scope(Feature = "FinanceSpendAnalyzer")]
        public static void AfterFeatureSteps()
        {
            Driver.BrowserTeardown();
        }

        [AfterStep]
        public static void AfterStepLogin()
        {
            if(ScenarioContext.Current.TestError!=null)
            {

            }
        }
    }
}
