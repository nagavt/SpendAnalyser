﻿using System;
using IngFundAnalyzerTestFramework.Pages;
using TechTalk.SpecFlow;
using System.Threading;
using IngFundAnalyzerTestFramework.Fixures;

namespace IngRunner.Bindings
{
    [Binding]
    public class FinanceSpendAnalyzerSteps
    {
        Home homePage = new Home();
        Login loginPage = new Login();
        Payments paymentsPage = new Payments();

        [Given(@"custom logins to Finance Spend Analyzer Application using Credentials '(.*)' '(.*)'")]
        public void GivenCustomLoginsToFinanceSpendAnalyzerApplicationUsingCredentials(string userName, string password)
        {
            /*
            loginPage.LaunchHomePage();
            Thread.Sleep(Configs.LazyLoadTime);
            loginPage.UserLogin(userName, password);
            Thread.Sleep(Configs.LazyLoadTime);
            ScenarioContext.Current.Add("userName", userName);
            ScenarioContext.Current.Add("password", password);
            */
        }
        
        [Given(@"he has closing balance '(.*)'")]
        public void GivenHeHasClosingBalance(int amount)
        {
            //string userName = ScenarioContext.Current.Get<string>("userName");
            string userName = Configs.UserName;
            Console.WriteLine($"Need to execute SQL to make Closing Balance as {amount} for User Name: {userName}");
        }
        
        [Given(@"he has done few transaction last (.*) months like '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' '(.*)'")]
        public void GivenHeHasDoneFewTransactionLastMonthsLike(int p0, int p1, int p2, int p3, int p4, int p5, int p6, int p7, int p8, int p9)
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"Enter Payment detais '(.*)' '(.*)' and '(.*)'")]
        [When(@"Enter Invalid Payment detais for any one field from '(.*)' '(.*)' and '(.*)'")]
        public void WhenEnterPaymentDetaisAnd(string transactionType, string amount, string description)
        {
            homePage.NavigateToPaymentsTab();
            Thread.Sleep(Configs.LazyLoadTime);
            paymentsPage.EnterPaymentDetails(transactionType, amount, description);
            
            ScenarioContext.Current.Add("transactionType", transactionType);
            ScenarioContext.Current.Add("amount", amount);
            ScenarioContext.Current.Add("description", description);
        }
        
        [When(@"Save the Payment details")]
        public void WhenSaveThePaymentDetails()
        {
            paymentsPage.SavePaymentDetails();
            Thread.Sleep(Configs.LazyLoadTime);
        }
        
        [When(@"he has done payment with details '(.*)' '(.*)' and '(.*)'")]
        public void WhenHeHasDonePaymentWithDetailsAnd(string p0, int p1, string p2)
        {
            
        }
        
        [When(@"he navigated to Transaction History Page")]
        public void WhenHeNavigatedToTransactionHistoryPage()
        {
            paymentsPage.NavigateToPaymentsHistoryFromPaymentStatusPage();
            Thread.Sleep(Configs.LazyLoadTime);
        }
        
        [Then(@"it should save Payment details")]
        public void ThenItShouldSavePaymentDetails()
        {
            Console.WriteLine("Call API and validate the payment details in database");
        }
        
        [Then(@"it displays message '(.*)'")]
        public void ThenItDisplaysMessage(string message)
        {
            //paymentsPage.ValidatePaymentSuccessMessageWith(message);
        }
        
        [Then(@"it should not save Payment details")]
        public void ThenItShouldNotSavePaymentDetails()
        {
            Console.WriteLine("Call API and validate that payment details are not saved in database");
        }
        
        [Then(@"it should display above Payment details in Transaction History Page")]
        public void ThenItShouldDisplayAbovePaymentDetailsInTransactionHistoryPage()
        {
            string transactionType=ScenarioContext.Current.Get<string>("transactionType");
            string amount = ScenarioContext.Current.Get<string>("amount");
            string description = ScenarioContext.Current.Get<string>("description");
            paymentsPage.ValidateDetailsInPaymentHistory(amount);
        }
        
        [Then(@"it should display above Aggregated data in Aggregated Transaction page")]
        public void ThenItShouldDisplayAboveAggregatedDataInAggregatedTransactionPage()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
