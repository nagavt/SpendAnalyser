﻿Feature: WebService
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

Scenario Outline: Call Login API
	Given I have Login API Available
	When I call Login API with Post Data using '<LoginId>' '<Password>'
	Then Login Response should be successful
Examples: 
| LoginId | Password | 
| abc     | 1234      |

Scenario Outline: Validate Login API Response
	Given I have Login API Available
	When I call Login API with Post Data using '<LoginId>' '<Password>'
	Then Login response Status should contains status '<StatusCode>' and Name '<Name>'
Examples: 
| LoginId | Password | StatusCode | Name  |
| abc     | 123      | 1          | Hello |

Scenario Outline: Call Payment API
	Given I have Payment API Available
	When I call Payment API with Post Data using <CustId> '<Amount>' '<PaymentType>' '<Description>'
	Then Payment Response should be successful
Examples: 
| CustId | Amount | PaymentType | Description |
| 1000    | 12    | CREDIT      | SAMPLE      |