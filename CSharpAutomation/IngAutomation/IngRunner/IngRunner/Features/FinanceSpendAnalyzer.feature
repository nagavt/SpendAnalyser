﻿Feature: FinanceSpendAnalyzer
	In order to analyze Financial Spends
	As a customer
	I want to do payments and analyze the spends in periodic manner

Scenario Outline: Make and Receive Payments
	Given custom logins to Finance Spend Analyzer Application using Credentials '<UserName>' '<Password>'
	And he has closing balance '<ClosingBalance>'
	When Enter Payment detais '<Type>' '<Amt>' and '<Desc>'
	And Save the Payment details
	Then it should save Payment details
	And it displays message '<Message>'
Examples: 
	| Type   | Amt   | Desc                       | ClosingBalance | Message  | UserName | Password |
	| Credit | 10000 | Purchased in shoppers stop | 0              | Hi Hello | abc      | 123      |
	| Debit  | 500   | Purchased in shoppers stop | 0              | Hi Hello | abc      | 123      |

Scenario Outline: Payment Page Field Validations
	Given custom logins to Finance Spend Analyzer Application using Credentials '<UserName>' '<Password>'
	And he has closing balance '<ClosingBalance>'
	When Enter Payment detais '<Type>' '<Amt>' and '<Desc>'
	And Save the Payment details
	Then it should not save Payment details
	And it displays message '<Message>'
Examples: 
	| Type   | Amt   | Desc                       | ClosingBalance | Message                          |UserName|Password|
	| Debit  | 10000 | Purchased in shoppers stop | 0              | You do not have suffient balance |UserName1|Password|
	| Debit  |       | Purchased in shoppers stop | 10000          | Missing Mandatory field          |UserName1|Password|
	| Credit | none  | Purchased in shoppers stop | 10000          | Invalid Amount                   |UserName1|Password|
	| Debit  | -200  | Purchased in shoppers stop | 10000          | Invalid Amount                   |UserName1|Password|

Scenario Outline: View Transaction History
	Given custom logins to Finance Spend Analyzer Application using Credentials '<UserName>' '<Password>'
	And he has closing balance '<ClosingBalance>'
	When Enter Payment detais '<Type>' '<Amt>' and '<Desc>'
	And Save the Payment details
	And he navigated to Transaction History Page
	Then it should display above Payment details in Transaction History Page
Examples: 
	| Type   | Amt | Desc                       | ClosingBalance | UserName | Password |
	| Credit | 100 | Purchased in shoppers stop | 0              | abc      | 123      |
	| Debit  | 100 | Purchased in shoppers stop | 10000          | abc      | 123      |

Scenario Outline: Viewing Aggregated Transaction Data
	Given custom logins to Finance Spend Analyzer Application using Credentials '<UserName>' '<Password>'
	And he has closing balance '<ClosingBalance>'
	And he has done few transaction last 3 months like '<Month1>' '<M1_TotalIncoming>' '<M1_TotalOutgoing>' '<Month2>' '<M2_TotalIncoming>' '<M2_TotalOutgoing>' '<Month3>' '<M3_TotalIncoming>' '<M3_TotalOutgoing>'
	Then it should display above Aggregated data in Aggregated Transaction page
Examples: 
	| ClosingBalance | Month1 | M1_TotalIncoming | M1_TotalOutgoing | Month2 | M2_TotalIncoming | M2_TotalOutgoing | Month3 | M3_TotalIncoming | M3_TotalOutgoing | UserName | Password |
	| 10000          | 1      | 100              | 100              | 1      | 100              | 100              | 1      | 100              | 100              | abc      | 123      |