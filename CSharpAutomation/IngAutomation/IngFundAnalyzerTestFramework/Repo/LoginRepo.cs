﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace IngFundAnalyzerTestFramework.Repo
{
    public class LoginRepo
    {
        public By UserName => By.Id("input-1");
        public By Password => By.XPath("//*[@id='input-2']");
        public By LoginButton => By.Id("submit");

        public By Root1 => By.TagName("spend-analyser");
        public By Root2 => By.CssSelector("login-form");
        public By Root3 => By.CssSelector("paper-input");
        public By Root4 => By.CssSelector("iron-input");

        //public string UserNameRootPath => "spend-analyser:css,login-form,paper-input,iron-input,input";
        public string UserNameRootPath => "spend-analyser,login-form,username:id,iron-input,input";
        //public string PasswordRootPath => "spend-analyser:css,login-form,paper-input,input";
        public string PasswordRootPath => "spend-analyser:css,login-form,password:id,iron-input,input";

        public string LoginButtoneRootPath => "spend-analyser:css,login-form,iron-form,paper-button";
    }
}
