﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace IngFundAnalyzerTestFramework.Repo
{
    public class HomeRepo
    {
        public By TransactionHistoryTab => By.Id("HistoryTab");

        public string PaymentsTabRootPath => "spend-analyser,common-header,div,a";
        //public string PaymentsTabRootPath => "spend-analyser";
        public string PaymentsTabTagName => "a";
    }
}
