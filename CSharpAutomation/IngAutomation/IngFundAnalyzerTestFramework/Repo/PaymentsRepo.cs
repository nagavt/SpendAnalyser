﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace IngFundAnalyzerTestFramework.Repo
{
    public class PaymentsRepo
    {
        public By TransType => By.Id("lst_TransType");
        public By Amt => By.Id("txtAmount");
        public By Desc => By.Id("txtDesc");
        public By Submit => By.Id("btnSubmit");
        public By PaymentSubmitMessage => By.Id("");

        public string TransTypeRootPath => "spend-analyser,make-payment,paper-dropdown-menu,paper-menu-button,iron-icon";

        //public string TransTypeListItemRootPath => "spend-analyser,make-payment,paper-dropdown-menu,paper-item";
        public string TransTypeListItemRootPath => "spend-analyser,make-payment,paper-dropdown-menu";

        public string AmountRootPath => "spend-analyser,make-payment,paper-input,paper-input-container,input";

        public string DescriptionRootPath => "spend-analyser,make-payment,paper-input,paper-input-container,iron-input,input";

        public string SubmitRootPath => "spend-analyser,make-payment,paper-button";

        public string PaymentStatusMessage => "spend-analyser,payment-status:h2";

        public string ErrorMessage => "spend-analyser,make-payment,paper-input,paper-input-error";

        //public string PaymentHistoryButton => "spend-analyser,payment-status,iron-form,transactionButton:id";
        public string PaymentHistoryButton => "spend-analyser,payment-status,iron-form";

        //public string PaymentHistoryGridData => "spend-analyser,iron-pages,payment-history,vaadin-grid";
        public string PaymentHistoryGridData => "spend-analyser,payment-history,vaadin-grid";
    }
}
