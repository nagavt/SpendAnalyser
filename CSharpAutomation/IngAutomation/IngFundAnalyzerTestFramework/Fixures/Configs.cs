﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngFundAnalyzerTestFramework.Fixures
{
    public static class Configs
    {
        //public static string IngUrl => @"https://developer.ing.com/openbanking/";
        public static string IngUrl => @"http://localhost:8081/login";
        public static string UserName => @"abc";
        public static string Password => @"123";
        public static string ApiUrl => @"http://localhost:8080";
        public static string LoginApiParams => @"/user/login";
        public static string PaymentApiParams => @"/user/1000/makepayment";

        public static double ImplicitWaitTime = 15;
        public static double PageLoadTime = 30;
        public static int LazyLoadTime = 8000;

        public static string DbServerName = "localhost";
        public static string DbName = "test";
        public static string DbUserName = "sa";
        public static string DbPassword = "MySql01*";
    }
}
