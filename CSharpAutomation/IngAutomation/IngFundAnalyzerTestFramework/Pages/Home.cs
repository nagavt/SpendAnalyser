﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IngFundAnalyzerTestFramework.CommonModules;
using IngFundAnalyzerTestFramework.Repo;
using OpenQA.Selenium;

namespace IngFundAnalyzerTestFramework.Pages
{
    public class Home
    {
        HomeRepo repo = new HomeRepo();

        public void NavigateToPaymentsTab()
        {
            //Driver.ClickElement(repo.PaymentsTab);
            //Driver.ClickJSShadowElement(Driver.driver.FindElement(repo.PaymentsTab));

            IWebElement webElement = Driver.FindElementInsideShadowRoot(repo.PaymentsTabRootPath);
            webElement.Click();
        }

        public void NavigateToTransactionHistoryTab()
        {
            Driver.ClickElement(repo.TransactionHistoryTab);
        }
    }
}
