﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IngFundAnalyzerTestFramework.Fixures;
using IngFundAnalyzerTestFramework.Repo;
using IngFundAnalyzerTestFramework.CommonModules;
using OpenQA.Selenium;

namespace IngFundAnalyzerTestFramework.Pages
{
    public class Login
    {
        LoginRepo repo = new LoginRepo();

        public void LaunchHomePage()
        {
            Driver.NavigateToUrl(Configs.IngUrl);
        }

        public void UserLogin(string userName, string password)
        {
            IWebElement webElement = Driver.FindElementInsideShadowRoot(repo.UserNameRootPath);
            webElement.SendKeys(userName);
            //Driver.HighlightElement(webElement);
            webElement = Driver.FindElementInsideShadowRoot(repo.PasswordRootPath);
            webElement.SendKeys(password);
            webElement = Driver.FindElementInsideShadowRoot(repo.LoginButtoneRootPath);
            //webElement.Click();
            Driver.ClickJSElement(webElement);
        }
    }
}
