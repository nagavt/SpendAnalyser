﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IngFundAnalyzerTestFramework.CommonModules;
using IngFundAnalyzerTestFramework.Repo;
using OpenQA.Selenium;
using System.Threading;

namespace IngFundAnalyzerTestFramework.Pages
{
    public class Payments
    {
        PaymentsRepo repo = new PaymentsRepo();

        public void SelectTransactionType(string transactionType)
        {
            try
            {
                IWebElement webElement = Driver.FindElementInsideShadowRoot(repo.TransTypeRootPath);
                Driver.ClickJSElement(webElement);
                Thread.Sleep(1000);
                /*webElement = Driver.FindElementInsideShadowRoot(repo.TransTypeListItemRootPath);
                Driver.ClickJSElement(webElement);*/
                webElement = Driver.FindElementInsideShadowRootExpandAll(repo.TransTypeListItemRootPath);
                var dropdownValue = webElement.FindElements(By.CssSelector("paper-item"));
                if (transactionType.ToLower().Trim() == "credit")
                {

                    //Driver.ClickJSElement(dropdownValue[1]);
                    Driver.ClickElement(dropdownValue[1]);
                }
                else
                {
                    //Driver.ClickJSElement(dropdownValue[0]);
                    Driver.ClickElement(dropdownValue[0]);
                }
            }
            catch
            { }
        }

        public void EnterPaymentDetails(string transactionType, string amount, string description)
        {
            SelectTransactionType(transactionType);
            IWebElement webElement = Driver.FindElementInsideShadowRoot(repo.AmountRootPath);
            webElement.Click();
            webElement.SendKeys(amount);
        }

        public void SavePaymentDetails()
        {
            IWebElement webElement = Driver.FindElementInsideShadowRootExpandAll(repo.SubmitRootPath);
            Driver.ClickJSElement(webElement);
        }

        public void ValidatePaymentSuccessMessageWith(string successMessage)
        {
            IWebElement webElement = Driver.FindElementInsideShadowRoot(repo.PaymentStatusMessage);
            //var data = webElement.FindElements(By.CssSelector("h2"));
            string actualMessage = Driver.GetLabel(webElement);
            //string actualMessage = Driver.GetLabel(repo.PaymentSubmitMessage);

            if (actualMessage.Trim() != successMessage.Trim())
            {
                throw new Exception($"Success Message {successMessage} is not displayed. Actual Message is: {actualMessage}");
            }
        }

        public void NavigateToPaymentsHistoryFromPaymentStatusPage()
        {
            //IWebElement webElement = Driver.FindElementInsideShadowRoot(repo.PaymentHistoryButton);
            IWebElement webElement = Driver.FindElementInsideShadowRootExpandAll(repo.PaymentHistoryButton);
            var data = webElement.FindElements(By.CssSelector("paper-button"));
            Driver.ClickJSElement(data[1]);
        }

        public void ValidateDetailsInPaymentHistory(string amount)
        {
            //Driver.HighlightElement(webElement);
            IWebElement webElement = Driver.FindElementInsideShadowRootExpandAll(repo.PaymentHistoryGridData);
            var data = webElement.FindElements(By.CssSelector("vaadin-grid-cell-content"));
            
            for(int i=0;i<data.Count;i++)
            {
                Console.WriteLine(data[i]);
                if (data[i].Text.Trim().Contains(amount))
                {
                    Driver.HighlightElement(data[i]);
                    break;
                }
            }
        }
    }
}
