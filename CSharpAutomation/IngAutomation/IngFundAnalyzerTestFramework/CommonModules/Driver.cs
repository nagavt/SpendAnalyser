﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using IngFundAnalyzerTestFramework.Fixures;
using System.Threading;
using System.Diagnostics;
using IngFundAnalyzerTestFramework.Pages;

namespace IngFundAnalyzerTestFramework.CommonModules
{
    public static class Driver
    {
        public static IWebDriver driver;

        static Home homePage = new Home();
        static Login loginPage = new Login();
        static Payments paymentsPage = new Payments();

        public static IWebElement root1;
        public static IWebElement shadowRoot1;
        public static IWebElement shadowRoot2;

        public static IKeyboard keyboard;

        public static void Main()
        {
            BrowserSetup();

            loginPage.LaunchHomePage();
            Thread.Sleep(Configs.LazyLoadTime);
            loginPage.UserLogin("abc", "123");
            Thread.Sleep(Configs.LazyLoadTime);

            paymentsPage.EnterPaymentDetails("CREDIT", "20", "description1");

            paymentsPage.SavePaymentDetails();
            Thread.Sleep(Configs.LazyLoadTime*2);
            //paymentsPage.ValidatePaymentSuccessMessageWith("Successful");

            BrowserTeardown();
        }

        public static void BrowserSetup()
        {
            foreach (var process in Process.GetProcessesByName("chromedriver"))
            {
                process.Kill();
            }
            //Thread.Sleep(2000);
            string currentFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            driver = new ChromeDriver(@"C:\\Data\\WebDriver");
            //driver = new InternetExplorerDriver(@"C:\\Data\\WebDriver");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().PageLoad=TimeSpan.FromSeconds(Configs.PageLoadTime);
            driver.Manage().Timeouts().ImplicitWait=TimeSpan.FromSeconds(Configs.ImplicitWaitTime);

            loginPage.LaunchHomePage();
            Thread.Sleep(Configs.LazyLoadTime);
            loginPage.UserLogin(Configs.UserName, Configs.Password);
            Thread.Sleep(Configs.LazyLoadTime);
        }

        public static void BrowserTeardown()
        {
            //driver.Quit();
            foreach (var process in Process.GetProcessesByName("chromedriver"))
            {
                process.Kill();
            }
        }

        public static void NavigateToUrl(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public static void ClickElement(By element)
        {
            IWebElement webElement = driver.FindElement(element);
            webElement.Click();
        }

        public static void ClickElement(IWebElement webElement)
        {
            webElement.Click();
        }

        public static void EnterText(By element,string text)
        {
            if (text != "")
            {
                IWebElement webElement = driver.FindElement(element);
                webElement.SendKeys(text);
            }
        }

        public static void ClickJSElement(By element)
        {
            IWebElement webElement = driver.FindElement(element);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click();", webElement);
        }

        public static void ClickJSElement(IWebElement webElement)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click();", webElement);
        }

        public static void EnterJSElement(By element, string value)
        {
            IWebElement webElement = driver.FindElement(element);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click();", webElement);
            //js.ExecuteScript("arguments[0].scrollIntoView(true)", webElement);
            js.ExecuteScript("arguments[1].value = arguments[0]; ", value, webElement);
        }

        public static void EnterJSElement(IWebElement webElement, string value)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click();", webElement);
            //js.ExecuteScript("arguments[0].scrollIntoView(true)", webElement);
            js.ExecuteScript("arguments[1].value = arguments[0]; ", value, webElement);
        }

        public static void GetJsText(IWebElement webElement)
        {
            String script = "return arguments[0].value";
            string textValue = (String)((IJavaScriptExecutor)driver).ExecuteScript(script, webElement);
            Console.WriteLine($"User Name is: {textValue}");
        }

        public static void SelectDropDownValue(By element,string value)
        {
            SelectElement custIdSelect = new SelectElement(driver.FindElement(element));
            custIdSelect.SelectByText(value);
        }

        public static void SelectDropDownValue(IWebElement webElement, string value)
        {
            SelectElement custIdSelect = new SelectElement(webElement);
            custIdSelect.SelectByText(value);
        }

        public static string GetCurrentTtile()
        {
            string currentTitle= driver.Title;
            return currentTitle;
        }

        public static string GetLabel(By element)
        {
            string msgCaption = "";
            msgCaption=driver.FindElement(element).Text;
            return msgCaption;
        }

        public static string GetLabel(IWebElement webElement)
        {
            string msgCaption = "";
            msgCaption = webElement.Text;
            return msgCaption;
        }

        public static void HighlightElement(By element)
        {
            var javaScriptDriver = (IJavaScriptExecutor)driver;
            IWebElement webElement = driver.FindElement(element);
            //string highlightJavascript = @"arguments[0].style.cssText = ""border-width: 3px; border-style: solid; border-color: green"";";
            string highlightJavascript = @"arguments[0].style.cssText = ""border-style: solid; border-color: red"";";
            javaScriptDriver.ExecuteScript(highlightJavascript, new object[] { webElement });
        }

        public static void HighlightElement(IWebElement webElement)
        {
            var javaScriptDriver = (IJavaScriptExecutor)driver;
            string highlightJavascript = @"arguments[0].style.cssText = ""border-style: solid; border-color: red"";";
            javaScriptDriver.ExecuteScript(highlightJavascript, new object[] { webElement });
        }

        public static IWebElement ExpandRootElement(IWebElement element)
        {
            IWebElement ele = (IWebElement)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].shadowRoot", element);
            return ele;
        }

        public static IWebElement DriverElementOfType(string value,string selectorType)
        {
            IWebElement webElement = null ;
            if (selectorType=="css")
                webElement= Driver.driver.FindElement(By.CssSelector(value));
            else if (selectorType == "id")
                webElement= Driver.driver.FindElement(By.Id(value));
            else if (selectorType == "name")
                webElement= Driver.driver.FindElement(By.Name(value));
            else if (selectorType == "tag")
                webElement= Driver.driver.FindElement(By.TagName(value));
            else if (selectorType == "class")
                webElement = Driver.driver.FindElement(By.ClassName(value));

            return webElement;
        }

        public static IWebElement ShadowElementOfType(string value, string selectorType)
        {
            IWebElement webElement = null;
            if (selectorType == "css")
                webElement = Driver.shadowRoot1.FindElement(By.CssSelector(value));
            else if (selectorType == "id")
                webElement = Driver.shadowRoot1.FindElement(By.Id(value));
            else if (selectorType == "name")
                webElement = Driver.shadowRoot1.FindElement(By.Name(value));
            else if (selectorType == "tag")
                webElement = Driver.shadowRoot1.FindElement(By.TagName(value));
            else if (selectorType == "class")
                webElement = Driver.shadowRoot1.FindElement(By.ClassName(value));


            return webElement;
        }

        public static IWebElement FindElementInsideShadowRoot(string shadowRoots)
        {
            string[] sres = shadowRoots.Split(',');

            for (int i = 0; i < sres.Length-2; i++)
            {
                string[] sresi = sres[i].Split(':');

                string selectoryType = "css";

                if (sresi.Length == 2)
                    selectoryType = sresi[1];

                if (i == 0)
                {
                    //root1 = Driver.driver.FindElement(By.CssSelector(sres[i]));
                    root1 = DriverElementOfType(sresi[0], selectoryType);
                    shadowRoot1 = Driver.ExpandRootElement(Driver.root1);
                }
                else
                {
                    //root1 = Driver.shadowRoot1.FindElement(By.CssSelector(sres[i]));
                    root1 = ShadowElementOfType(sresi[0], selectoryType);
                    shadowRoot1 = Driver.ExpandRootElement(Driver.root1);
                }
            }
            root1 = Driver.shadowRoot1.FindElement(By.CssSelector(sres[sres.Length - 2]));
            return root1.FindElement(By.CssSelector(sres[sres.Length-1]));
        }

        public static IWebElement FindElementInsideShadowRootExpandAll(string shadowRoots)
        {
            string[] sres = shadowRoots.Split(',');

            for (int i = 0; i < sres.Length - 1; i++)
            {
                string[] sresi = sres[i].Split(':');

                string selectoryType = "css";

                if (sresi.Length == 2)
                    selectoryType = sresi[1];

                if (i == 0)
                {
                    //root1 = Driver.driver.FindElement(By.CssSelector(sres[i]));
                    root1 = DriverElementOfType(sresi[0], selectoryType);
                    shadowRoot1 = Driver.ExpandRootElement(Driver.root1);
                }
                else
                {
                    //root1 = Driver.shadowRoot1.FindElement(By.CssSelector(sres[i]));
                    root1 = ShadowElementOfType(sresi[0], selectoryType);
                    shadowRoot1 = Driver.ExpandRootElement(Driver.root1);
                }
            }
            return shadowRoot1.FindElement(By.CssSelector(sres[sres.Length - 1]));
        }

        public static void ClickTab()
        {
            keyboard.PressKey(Keys.Tab);
            keyboard.PressKey(Keys.Tab);
            keyboard.PressKey(Keys.Enter);
        }
    }
}
