﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IngFundAnalyzerTestFramework.CommonModules
{


    public enum Verb
    {
        GET,
        POST,
        PUT,
        DELETE
    }

    class LoginClass
    {
        public string loginId { get; set; }
        public string password { get; set; }
    }

    class LoginResponseClass
    {
        public string status { get; set; }
        public CustomerData data { get; set; }
    }


    class CustomerData
    {
        public string customerId { get; set; }
        public string loginId { get; set; }
        public string name { get; set; }
    }

    class PaymentClass
    {
        public int customerId { get; set; }
        public int amount { get; set; }
        public string paymentType { get; set; }
        public string description { get; set; }
    }

    class PaymentResponse
    {
        public string status { get; set; }
        public PaymentResponseData paymentData { get; set; }
    }

    class PaymentResponseData
    {
        public int transactionId { get; set; }
        public string transactionDate { get; set; }
        public string description { get; set; }
        public int amount { get; set; }
        public string paymentType { get; set; }
        public int closingBalance { get; set; }
    }

    public class RestClient
    {
        public string EndPoint { get; set; }
        public Verb Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }
        public HttpStatusCode responseCode { get; set; }
        public string responseValue { get; set; }

        string loginInputJsonFileName = "LoginInputJson.json";
        string loginResponseJsonFileName = "LoginResponseJson.json";
        string paymentInputJsonFileName = "PaymentInputJson.json";
        string paymentResponseJsonFileName = "PaymentResponseJson.json";

        public RestClient()
        {
            EndPoint = "";
            Method = Verb.GET;
            ContentType = "application/JSON";
            PostData = "";
        }

        public RestClient(string endpoint, Verb method, string postData)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "text/json";
            PostData = postData;
        }

        public string Request(string parameters)

        {

            var request = (HttpWebRequest)WebRequest.Create(EndPoint + parameters);

            request.Method = Method.ToString();

            request.ContentLength = 0;

            request.ContentType = ContentType;

            using (var response = (HttpWebResponse)request.GetResponse())

            {

                responseValue = string.Empty;

                responseCode = response.StatusCode;

                if (response.StatusCode != HttpStatusCode.OK)

                {

                    var message = String.Format("Faile: Received HTTP {0}", response.StatusCode);

                    throw new ApplicationException(message);

                }

                using (var responseStream = response.GetResponseStream())

                {

                    if (responseStream != null)

                        using (var reader = new StreamReader(responseStream))

                        {

                            responseValue = reader.ReadToEnd();

                        }

                }

                var Currentfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                var FileName = Path.Combine(Currentfolder, "LoginResponse.json");
                File.WriteAllText(FileName, responseValue);

                return responseValue;

            }



        }

        public void ReadLoginResponse()
        {
            var Currentfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var FileName = Path.Combine(Currentfolder, loginResponseJsonFileName);
            string jsonFileContent = File.ReadAllText(FileName);
            dynamic stuff = JsonConvert.DeserializeObject(jsonFileContent);
            //Console.WriteLine("json deserialized value is" + stuff.RestResponse.result[1].country);
        }

        public void ReadPaymentResponse()
        {
            var Currentfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var FileName = Path.Combine(Currentfolder, paymentResponseJsonFileName);
            string jsonFileContent = File.ReadAllText(FileName);
            dynamic stuff = JsonConvert.DeserializeObject(jsonFileContent);
            //Console.WriteLine("json deserialized value is" + stuff.RestResponse.result[1].country);
        }

        public void ValidateLoginResponse(string statusCode,string name)
        {
            var Currentfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var FileName = Path.Combine(Currentfolder, loginResponseJsonFileName);
            string jsonFileContent = File.ReadAllText(FileName);
            //List<LoginResponseClass> items = JsonConvert.DeserializeObject<List<LoginResponseClass >> (jsonFileContent);

            LoginResponseClass items = Newtonsoft.Json.JsonConvert.DeserializeObject<LoginResponseClass>(jsonFileContent);


            string actualStatusCode = items.status;
            CustomerData customers = items.data;
            string actualName = customers.name;
            

            Console.WriteLine("json deserialized value for status is: " + actualStatusCode);
            Console.WriteLine("json deserialized value for name is: " + actualName);

            if(actualName!=name)
            {
                throw new Exception($"Name is not matching. Actual Name is: {actualName}");
            }
        }

        public string BuildLoginJson(string userName, string password)
        {
            LoginClass loginClass = new LoginClass();

            loginClass.loginId = userName;
            loginClass.password = password;

            var json = JsonConvert.SerializeObject(loginClass);

            var Currentfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var FileName = Path.Combine(Currentfolder, loginInputJsonFileName);
            File.WriteAllText(FileName, json);

            return FileName;
        }

        public string BuildPaymentJson(int custId, int amt, string type,  string desc)
        {
            PaymentClass jsonClass = new PaymentClass();

            jsonClass.customerId = custId;
            jsonClass.amount = amt;
            jsonClass.paymentType = type;
            jsonClass.description = desc;

            var json = JsonConvert.SerializeObject(jsonClass);

            var Currentfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var FileName = Path.Combine(Currentfolder, paymentInputJsonFileName);
            File.WriteAllText(FileName, json);

            return FileName;
        }


        public void PostLoginAPI(string url)
        {
            RestApiPost(url, loginInputJsonFileName, loginResponseJsonFileName);
        }

        public void PostPaymentAPI(string url)
        {
            RestApiPost(url, paymentInputJsonFileName, paymentResponseJsonFileName);
        }

        public void RestApiPost(string url,string inputFileName,string outputFileName)
        {
            try
            {
                var httpWebRequest =
                    (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                string Currentfolder = "";
                string FileName = "";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    Currentfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                    FileName = Path.Combine(Currentfolder, inputFileName);
                    string json = File.ReadAllText(FileName);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                responseCode = httpResponse.StatusCode;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Console.WriteLine(result);
                    Currentfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                    FileName = Path.Combine(Currentfolder, outputFileName);
                    File.WriteAllText(FileName, result);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public static bool PostMultiPartAPI(string url, string filePath)
        {
            if (!File.Exists(filePath))
            {
                Console.WriteLine("File does not exist");
                return false;
            }

            var file = new FileInfo(filePath);
            if (file.Length > 10485760) // TODO Get Actual Limit
            {
                Console.WriteLine("File too large");
                return false;
            }

            return PostMultiPart(url, file);
        }

        private static Boolean PostMultiPart(string restUrl, FileInfo filePath)
        {
            HttpWebResponse response = null;
            HttpWebRequest request = null;

            //string currentfolder = "";
            //string fileName = "";

            try
            {
                var boundary = string.Format("----------{0:N}", Guid.NewGuid());
                var content = new MemoryStream();
                var writer = new StreamWriter(content);

                var fs = new FileStream(filePath.FullName, FileMode.Open, FileAccess.Read);
                var data = new byte[fs.Length];
                fs.Read(data, 0, data.Length);
                fs.Close();

                writer.WriteLine("--{0}", boundary);
                writer.WriteLine("Content-Disposition: form-data; name=\"file\"; filename=\"{0}\"", filePath.Name);
                writer.WriteLine("Content-Type: application/octet-stream");
                writer.WriteLine();
                writer.Flush();

                content.Write(data, 0, data.Length);

                writer.WriteLine();

                writer.WriteLine("--" + boundary + "--");
                writer.Flush();
                content.Seek(0, SeekOrigin.Begin);

                request = WebRequest.Create(restUrl) as HttpWebRequest;
                if (request == null)
                {
                    Console.WriteLine("Unable to create REST query");
                    return false;
                }

                request.Method = "POST";
                request.ContentType = string.Format("multipart/form-data; boundary={0}", boundary);
                request.Accept = "application/json";
                request.Headers["Authorization"] =
                    "Basic " + Convert.ToBase64String(
                        Encoding.Default.GetBytes("EBI_Service_User:EBI_Service_User@123"));
                request.Headers.Add("X-Atlassian-Token", "nocheck");
                request.ContentLength = content.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    content.WriteTo(requestStream);
                    requestStream.Close();
                }

                using (response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var reader = new StreamReader(response.GetResponseStream());
                        Console.WriteLine("Server returned error");
                        return false;
                    }
                    return true;
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        var reader = new StreamReader(errorResponse.GetResponseStream());
                        Console.WriteLine("Server returned error");
                    }
                }

                if (request != null)
                {
                    request.Abort();
                }

                return false;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }
        }

    }
}
