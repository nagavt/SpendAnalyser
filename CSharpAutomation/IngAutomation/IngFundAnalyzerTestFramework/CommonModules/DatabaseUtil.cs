﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace IngFundAnalyzerTestFramework.CommonModules
{
    public static class DatabaseUtil
    {
        static SqlConnection con;

        public static bool DatabaseConnectionStatus(string serverName, string databaseName, string userName, string password)
        {
            try
            {
                var connetionString = $"Data Source={serverName};Initial Catalog={databaseName};User ID={userName};Password={password}";
                con = new SqlConnection(connetionString);
                con.Open();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Sql Connection error. {e.Message}");
                return false;
            }
        }

        public static void ExecuteNonQuery(string sql)
        {
            SqlCommand command = new SqlCommand(sql, con);
            command.ExecuteNonQuery();
        }
    }
}
